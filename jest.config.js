const patterns = [
  '@react-native',
  'react-native',
  '@react-navigation',
  'react-navigation-tabs',
  'react-native-vector-icons',
  'react-native-screens',
  'react-native-safe-area-context',
  'react-native-design-utility',
  'install',
  'graphql-config',
  'graphql-cli',
  'get-graphql-schema',
  '@react-navigation/native-stack',
  '@react-navigation/native',
  '@react-navigation/bottom-tabs',
];

module.exports = {
  verbose: true,
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  setupFiles: ['<rootDir>/jest-setup.js'],
  transformIgnorePatterns: [`node_modules/(?!(${patterns.join('|')})/)`],
  collectCoverage: true,
  collectCoverageFrom: ['**/*.tsx'],
  testPathIgnorePatterns: ['/node_modules/'],
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  coveragePathIgnorePatterns: ['/node_modules/'],
  coverageThreshold: {
    global: {
      statements: 50,
      branches: 25,
      functions: 28.38,
      lines: 50,
    },
  },
  transform: {
    '^.+\\.svg$': '<rootDir>/fileTransformer.js',
  },
};
