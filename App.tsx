import React from 'react';
import {ApolloProvider} from '@apollo/react-hooks';
import {UtilityThemeProvider} from 'react-native-design-utility';

import {NavigationContainer} from '@react-navigation/native';
import MainStackNavigator from './src/Navigator/MainStack';
import {theme} from './src/constant/theme';
import {client} from './src/graphql/client';

const App = () => {
  return (
    <UtilityThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <NavigationContainer>
          <MainStackNavigator />
        </NavigationContainer>
      </ApolloProvider>
    </UtilityThemeProvider>
  );
};

export default App;
