/* eslint-disable */
// import 'react-native-gesture-handler/jestSetup';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
    useRoute: () => ({
      name: '',
    })
  };
});

jest.mock('react-native-safe-area-context', () => {
  return {
    ...jest.requireActual('react-native-safe-area-context'),
    useSafeAreaInsets: () => ({
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    }),
  };
});

// jest.mock('react-native-reanimated', () => {
//   const Reanimated = require('react-native-reanimated/mock');

//   // The mock for `call` immediately calls the callback which is incorrect
//   // So we override it with a no-op
//   Reanimated.default.call = () => {};

//   return Reanimated;
// });

// jest.mock('react-native-keychain', () => {
//   return {
//     setGenericPassword: jest.fn((username, password) => {}),
//     getGenericPassword: jest.fn(() => {}),
//     resetGenericPassword: jest.fn(() =>  {}),
//   }
// });

// import mock from '@stripe/stripe-react-native/jest/mock.js';
// jest.mock('@stripe/stripe-react-native', () => mock);
