export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends {[key: string]: unknown}> = {[K in keyof T]: T[K]};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type FeedItem = {
  __typename?: 'FeedItem';
  pubDate: Scalars['String'];
  text: Scalars['String'];
  title: Scalars['String'];
  subtitle: Scalars['String'];
  description: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  summary: Scalars['String'];
  linkUrl: Scalars['String'];
  duration: Scalars['String'];
};

export type Podcast = {
  __typename?: 'Podcast';
  artist: Scalars['String'];
  podcastName: Scalars['String'];
  feedUrl: Scalars['String'];
  thumbnail: Scalars['String'];
  episodesCount: Scalars['Int'];
  genres: Array<Scalars['String']>;
};

export type SearchQuery = {
  search: Podcast[];
};

export type SearchQueryVariables = {
  term: string;
};
export type QueryFeedArgs = {
  feedUrl: Scalars['String'];
};
