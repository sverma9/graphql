import 'react-native';

import React from 'react';
import RenderSearchItems from '../__test__/../RenderItems';
import {render, screen} from '@testing-library/react-native';

const mockedPush = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({push: mockedPush}),
}));

describe('RenderSearchItems', () => {
  it('renders correctly', async () => {
    render(
      <RenderSearchItems
        item={{
          artist: 'Not Sorry Productions',
          podcastName: 'Harry Potter and the Sacred Text',
          feedUrl: 'https://rss.acast.com/harrypottersacredtext',
          thumbnail:
            'https://is5-ssl.mzstatic.com/image/thumb/Podcasts114/v4/03/3a/68/033a6816-4dae-df67-ef93-5c1f0580e4f3/mza_14515451638148560314.jpg/100x100bb.jpg',
          episodesCount: 341,
          genres: [
            'Religion & Spirituality',
            'Podcasts',
            'Society & Culture',
            'Philosophy',
          ],
        }}
      />,
    );
    const img = await screen.findByTestId('img');
    const podcastName = await screen.findByText(
      'Harry Potter and the Sacred Text',
    );
    const artist = await screen.findByText('Not Sorry Productions');
    const episodesCount = await screen.findByText('341');
    expect(img).toBeTruthy();
    expect(podcastName).toBeTruthy();
    expect(artist).toBeTruthy();
    expect(episodesCount).toBeTruthy();
  });
});
