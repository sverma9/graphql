/* eslint-disable react-native/no-inline-styles */
import {Image, TouchableOpacity} from 'react-native';
import React, {PropsWithChildren} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Box, Text} from 'react-native-design-utility';

import {theme} from '../constant/theme';
import {Podcast} from '../types/graphql';

type Props = {
  item: Podcast;
};

type PodCastDetailsParamList = {
  [x: string]: any;
  PodCastDetails: Podcast;
};

const RenderSearchItems: React.FC<Props> = (
  props: PropsWithChildren<Props>,
) => {
  const navigation = useNavigation<PodCastDetailsParamList>();
  return (
    <Box h={80} dir="row" align="center" px="sm">
      <Box h={70} w={70} radius={10} mr={10}>
        <Image
          testID="img"
          style={{height: 70, width: 70}}
          source={{
            uri: props.item.thumbnail,
          }}
        />
      </Box>
      <Box style={{width: '80%'}}>
        <Text
          bold
          size={theme.text.size.sm}
          style={{
            width: '100%',
            marginRight: 10,
          }}
          numberOfLines={2}
          ellipsizeMode="tail">
          {props.item.podcastName}
        </Text>
        <Text size={theme.text.size.xsm} color={theme.color.greenDarkest}>
          {props.item.artist}
        </Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('PodCastDetails', {data: props.item})
          }>
          <Text size={theme.text.size.xsm} color={theme.color.blueLightest}>
            {props.item.episodesCount}
          </Text>
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default RenderSearchItems;
