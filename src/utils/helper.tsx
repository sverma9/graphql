import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Octicons from 'react-native-vector-icons/Octicons';

interface Props {
  name: any;
}

function getIcons(name?: Props) {
  if (Octicons.hasIcon(name)) {
    return Octicons;
  }
  if (AntDesign.hasIcon(name)) {
    return AntDesign;
  }
  if (Ionicons.hasIcon(name)) {
    return Ionicons;
  }
  if (Feather.hasIcon(name)) {
    return Feather;
  }
  if (MaterialCommunityIcons.hasIcon(name)) {
    return MaterialCommunityIcons;
  }
  if (Entypo.hasIcon(name)) {
    return Entypo;
  }
  return FontAwesomeIcon;
}

export {getIcons};
