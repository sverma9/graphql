import {Podcast} from '../types/graphql';

export type SearchStackRouteParamsList = {
  Search: undefined;
  PodCastDetails: {
    data: Podcast;
  };
};
