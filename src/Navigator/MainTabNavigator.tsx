import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import ListenNowScreen from '../component/ListenNowScreen/ListenScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LibraryScreen from '../component/Library/LibraryScreen';
import SearchScreen from '../component/Search/SearchScreen';
import PodCastDetails from '../component/PodCastDetails/PodCastDetails';
import {theme} from '../constant/theme';
import {getIcons} from '../utils/helper';

const MainTab = createBottomTabNavigator();
const Icon = getIcons();
const ListenNowStack = createNativeStackNavigator();
const LibraryStack = createNativeStackNavigator();
const SearchStack = createNativeStackNavigator();

const ListenNowStackNavigator = () => {
  return (
    <ListenNowStack.Navigator
      defaultScreenOptions={{
        title: 'Listen Now',
      }}>
      <ListenNowStack.Screen
        name="ListenNowScreen"
        component={ListenNowScreen}
      />
    </ListenNowStack.Navigator>
  );
};

const LibraryStackNavigator = () => {
  return (
    <LibraryStack.Navigator
      defaultScreenOptions={{
        title: 'Library',
      }}>
      <LibraryStack.Screen name="LibraryScreen" component={LibraryScreen} />
    </LibraryStack.Navigator>
  );
};

const SearchStackNavigator = () => {
  return (
    <SearchStack.Navigator
      defaultScreenOptions={{
        title: 'Search',
        headerTintColor: theme.color.blueLightest,
      }}>
      <SearchStack.Screen name="Search" component={SearchScreen} />
      <SearchStack.Screen
        name="PodCastDetails"
        component={PodCastDetails}
        options={{headerTitle: ''}}
      />
    </SearchStack.Navigator>
  );
};
const MainTabNavigator = () => {
  return (
    <MainTab.Navigator
      screenOptions={() => ({
        tabBarActiveTintColor: '#99CEEB',
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      })}>
      <MainTab.Screen
        name="Listen Now"
        component={ListenNowStackNavigator}
        options={{
          tabBarIcon: () => (
            <Icon name="headphones" size={24} color="#033F63" />
          ),
        }}
      />
      <MainTab.Screen
        name="Library"
        component={LibraryStackNavigator}
        options={{
          tabBarIcon: () => (
            <Icon name="stack-overflow" size={30} color="#033F63" />
          ),
        }}
      />
      <MainTab.Screen
        name="Search "
        component={SearchStackNavigator}
        options={{
          tabBarIcon: () => <Icon name="search" size={24} color="#033F63" />,
        }}
      />
    </MainTab.Navigator>
  );
};

export default MainTabNavigator;
