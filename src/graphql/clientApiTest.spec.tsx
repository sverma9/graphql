import {gql} from '@apollo/react-hooks';
import {ApolloServer} from 'apollo-server';

const typeDefs = gql`
  type Query {
    search(term: String): String!
  }
`;

const resolvers = {
  Query: {
    search: (_: any, {term}: any) => `${term}`,
  },
};

it('return Search Data', async () => {
  const testServer = new ApolloServer({
    typeDefs,
    resolvers,
  });

  const result = await testServer.executeOperation({
    query: 'query search($term: String) {search(term: $term)}',
    variables: {term: 'Jeo Mendes'},
  });

  expect(result.errors).toBeUndefined();
  expect(result.data?.search).toBe('Jeo Mendes');
});
