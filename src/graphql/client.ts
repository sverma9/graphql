import fetch from 'cross-fetch';
import {ApolloClient, InMemoryCache, createHttpLink} from '@apollo/client';

export const client = new ApolloClient({
  link: createHttpLink({
    uri: 'https://frozen-river-77426.herokuapp.com/query',
    fetch: fetch,
  }),
  cache: new InMemoryCache(),
});
