import {gql} from '@apollo/react-hooks';

const feedQuery = gql`
  query feedQuery($feedUrl: String!) {
    feed(feedUrl: $feedUrl) {
      description
      duration
      image
      linkUrl
      pubDate
      text
      title
    }
  }
`;

export default feedQuery;
