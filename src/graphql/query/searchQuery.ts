import {gql} from '@apollo/react-hooks';

const searchQuery = gql`
  query SearchQuery($term: String!) {
    search(term: $term) {
      artist
      episodesCount
      feedUrl
      podcastName
      thumbnail
      genres
    }
  }
`;

export default searchQuery;
