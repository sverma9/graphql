/* eslint-disable react/jsx-no-undef */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import {MockedProvider} from '@apollo/client/testing';
import {render, act, cleanup, screen, waitFor} from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import searchQuery from '../searchQuery';
import SearchScreen from '../../../component/Search/SearchScreen';

const mockedData = {
  request: {
    query: searchQuery,
  },
  result: {
    item: {
      artist: 'Jeo Mendes',
      episodesCount: 2,
      feedUrl: 'https://anchor.fm/s/3497767c/podcast/rss',
      podcastName: 'Jeo Mendes',
      thumbnail:
        'https://is2-ssl.mzstatic.com/image/thumb/Podcasts114/v4/5a/ab/08/5aab08ae-c6e7-9a3b-8acc-ec510a296e84/mza_17265899747659580931.jpg/100x100bb.jpg',
    },
  },
};
const mockErrorData = {
  request: {
    query: searchQuery,
  },
  error: new Error('NetWork Error'),
};
describe('SearchScreen', () => {
  afterEach(cleanup);
  it('render search item', async () => {
    let wrapper;
    await act(async () => {
      wrapper = renderer.create(
        <MockedProvider mocks={[mockedData]} addTypename={false}>
          <SearchScreen />
        </MockedProvider>,
      );
    });
    // await act(() => waitFor(() => {}))
    expect(await wrapper).toBeTruthy();
    await Promise.resolve();
  });
});

describe('onError ', () => {
  afterEach(cleanup);
  it('getErrorMsg', async () => {
    const {getByTestId, getByText} = render(
      <MockedProvider addTypename={false} mocks={[mockErrorData]}>
        <SearchScreen />
      </MockedProvider>,
    );
  });
});
