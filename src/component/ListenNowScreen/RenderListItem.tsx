import {Image, StyleSheet, View} from 'react-native';
import React from 'react';

interface Props {
  userId: Number;
  userName: String;
  image?: String;
}

const RenderListItem: React.FC<Props> = props => {
  const {image} = props;
  return (
    <View
      style={[props.image !== '' ? styles.listView : styles.emptyImg]}
      testID={'render-test'}>
      {props.image !== '' && (
        <Image
          testID="imgId"
          style={styles.tinyLogo}
          source={{
            uri: image,
          }}
        />
      )}
    </View>
  );
};

export default RenderListItem;

const styles = StyleSheet.create({
  listView: {
    height: 50,
    width: 50,
    margin: 10,
    borderRadius: 10,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  emptyImg: {
    height: 50,
    width: 50,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#DCD2D0',
  },
});
