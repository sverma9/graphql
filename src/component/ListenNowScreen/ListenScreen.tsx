import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  NativeScrollEvent,
  NativeSyntheticEvent,
} from 'react-native';
import RenderListItem from './RenderListItem';

const data = [
  {id: 1, name: 'axyz', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 2, name: 'abc', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 3, name: 'qwer', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 4, name: 'qwerty', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 5, name: 'qw', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {
    id: 6,
    name: 'tg',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8qpFl6dlwwAWXRvi15ZQej5TKATRxJ5Q3A7782EhmlSZsxI_oTNxjK0DY-1431uwp_a0&usqp=CAU',
  },
  {id: 7, name: 'bh', img: ''},
  {id: 8, name: 'bghn', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 9, name: 'mhg', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 10, name: 'amklpbc', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 11, name: 'zasx', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 12, name: 'tyuiop', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 13, name: 'axyz', img: ''},
  {id: 14, name: 'abc', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 15, name: 'qwer', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 16, name: 'qwerty', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 17, name: 'qw', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 18, name: 'tg', img: ''},
  {id: 19, name: 'bh', img: ''},
  {id: 20, name: 'bghn', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 21, name: 'mhg', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 22, name: 'amklpbc', img: 'https://reactnative.dev/img/tiny_logo.png'},
  {id: 23, name: 'zasx', img: ''},
  {id: 24, name: 'tyuiop', img: 'https://reactnative.dev/img/tiny_logo.png'},
];

const ListenNowScreen = () => {
  const handleScroll = (
    _event: NativeSyntheticEvent<NativeScrollEvent>,
  ): void => {};
  const renderItem = ({item}) => {
    return (
      <RenderListItem userId={item.id} userName={item.name} image={item.img} />
    );
  };

  const renderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        <ActivityIndicator size="small" />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View testID={'flatList'}>
        <Text>ListenNowScreen</Text>
        <FlatList
          data={data}
          keyExtractor={item => String(item.id)}
          onScroll={handleScroll}
          renderItem={renderItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={2}
          ListFooterComponent={renderLoader}
          onEndReachedThreshold={0}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  listView: {
    height: 50,
    width: 50,
    margin: 10,
    borderRadius: 10,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  emptyImg: {
    height: 50,
    width: 50,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#DCD2D0',
  },
  loaderStyle: {
    marginTop: 20,
  },
});

export default ListenNowScreen;
