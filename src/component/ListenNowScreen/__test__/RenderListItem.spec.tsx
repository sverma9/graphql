import {render, screen} from '@testing-library/react-native';
import React from 'react';
import RenderListItem from '../RenderListItem';

const data = {
  id: 1,
  name: 'axyz',
  img: 'https://reactnative.dev/img/tiny_logo.png',
};

describe('Render', () => {
  it('RenderListItem', async () => {
    render(
      <RenderListItem userId={data.id} userName={data.name} image={data.img} />,
    );

    const getImgId = await screen.getByTestId('imgId');
    expect(getImgId).toBeTruthy();
  });
});
