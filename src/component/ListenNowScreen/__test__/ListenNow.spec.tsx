import React from 'react';
import ListenNowScreen from '../ListenScreen';
import {fireEvent, render} from '@testing-library/react-native';
describe('Testing FlatList', () => {
  it('Scroll Event', async () => {
    const {debug, getByTestId} = render(<ListenNowScreen />);
    fireEvent(getByTestId('flatList'), 'onScroll', {
      nativeEvent: {
        contentSize: {height: 600, width: 400},
        contentOffset: {y: 150, x: 0},
        layoutMeasurement: {height: 100, width: 100},
      },
    });
    debug();
  });
});



// /TESTING