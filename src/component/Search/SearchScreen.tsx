import {useLazyQuery} from '@apollo/react-hooks';
import React, {useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItemInfo,
  StyleSheet,
  TextInput,
} from 'react-native';
import {Text, Box} from 'react-native-design-utility';
import RenderSearchItems from '../../Common/RenderItems';
import {theme} from '../../constant/theme';
import searchQuery from '../../graphql/query/searchQuery';
import {SearchQuery, SearchQueryVariables, Podcast} from '../../types/graphql';
import {getIcons} from '../../utils/helper';

const Icon = getIcons();

const SearchScreen = () => {
  const [term, setTerm] = useState<string>('');
  const [search, {data, loading, error}] = useLazyQuery<
    SearchQuery,
    SearchQueryVariables
  >(searchQuery);
  const onSearch = async () => {
    try {
      await search({variables: {term}});
      setTerm('');
    } catch (e) {
      console.log('E', e);
    }
  };
  const renderItem = (item: ListRenderItemInfo<Podcast>) => {
    return <RenderSearchItems item={item.item} />;
  };
  return (
    <Box f={1} bg="white">
      <Box h={40} w="100%" px="sm" my="sm">
        <Box
          dir="row"
          align="center"
          h={48}
          bg="greyLightest"
          radius={10}
          px="sm">
          <Icon name="search" size={20} color={theme.color.greyDarkest} />
          <TextInput
            testID="inputText"
            style={styles.input}
            placeholder="Search Song"
            selectionColor={theme.color.blueLight}
            onChangeText={setTerm}
            autoCorrect={false}
            onSubmitEditing={onSearch}
            value={term}
          />
        </Box>
      </Box>

      {error ? (
        <>
          <Box f={1} center testID="container">
            <Text testID="TextErrorMsg" color="red">
              {error.message}
            </Text>
          </Box>
        </>
      ) : (
        <FlatList<Podcast>
          testID="PodcastFlatList"
          keyboardShouldPersistTaps="never"
          contentContainerStyle={styles.listContentContainer}
          ListHeaderComponent={
            <>
              {loading && (
                <Box f={1} center h={300} testID="ActivityIndicator">
                  <ActivityIndicator
                    size="large"
                    color={theme.color.blueLightest}
                  />
                </Box>
              )}
            </>
          }
          style={styles.list}
          data={data?.search ?? []}
          renderItem={item => renderItem(item)}
          keyExtractor={item => String(item.feedUrl)}
          ListEmptyComponent={
            <>
              {!loading && (
                <Box f={1} center>
                  <Text color="grey"> No Podcast Found</Text>
                </Box>
              )}
            </>
          }
        />
      )}
    </Box>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    flex: 1,
    backgroundColor: theme.color.greyLightest,
    justifyContent: 'flex-start',
    borderRadius: 10,
    paddingHorizontal: theme.space.sm,
    fontSize: theme.space.sm,
  },
  list: {
    minHeight: '100%',
  },
  listContentContainer: {
    minHeight: '100%',
    paddingBottom: 90,
  },
});

export default SearchScreen;
