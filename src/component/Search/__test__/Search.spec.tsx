import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react-native';
import {MockedProvider} from '@apollo/client/testing';
import SearchScreen from '../SearchScreen';
import searchQuery from '../../../graphql/query/searchQuery';

const mockedData = {
  request: {
    query: searchQuery,
  },
  result: {
    item: {
      artist: 'Jeo Mendes',
      episodesCount: 2,
      feedUrl: 'https://anchor.fm/s/3497767c/podcast/rss',
      podcastName: 'Jeo Mendes',
      thumbnail:
        'https://is2-ssl.mzstatic.com/image/thumb/Podcasts114/v4/5a/ab/08/5aab08ae-c6e7-9a3b-8acc-ec510a296e84/mza_17265899747659580931.jpg/100x100bb.jpg',
    },
  },
};

const mockErrorData = {
  request: {
    query: searchQuery,
  },
  error: new Error('NetWork Error'),
};

describe('Testing FlatList', () => {
  it('Search Item', async () => {
    const {getByPlaceholderText, getByTestId} = render(
      <MockedProvider addTypename={false} mocks={[mockedData]}>
        <SearchScreen />
      </MockedProvider>,
    );
    const textInput = getByPlaceholderText('Search Song');
    const testFlatList = getByTestId('PodcastFlatList');

    const searchItem = 'Test';
    fireEvent.changeText(textInput, searchItem);
    expect(textInput).toBeTruthy();
    expect(testFlatList).toBeTruthy();
  });
});

describe('Error Testing', () => {
  it('Error View', async () => {
    render(
      <MockedProvider addTypename={false} mocks={[mockErrorData]}>
        <SearchScreen />
      </MockedProvider>,
    );
    const submitError = screen.queryAllByTestId('Network request failed');
    expect(submitError).toBeTruthy();
  });
});
// // ActivityIndicator
describe('Activity Loading', () => {
  it('Loading', () => {
    render(
      <MockedProvider addTypename={false} mocks={[mockedData]}>
        <SearchScreen />
      </MockedProvider>,
    );
    waitFor(() => screen.findByTestId('ActivityIndicator'), {
      timeout: 200,
      interval: 100,
    });
  });
});
