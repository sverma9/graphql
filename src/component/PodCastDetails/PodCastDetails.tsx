/* eslint-disable react-native/no-inline-styles */
import {FlatList, Image, StyleSheet} from 'react-native';
import React from 'react';
import {RouteProp, useRoute} from '@react-navigation/native';
import {SearchStackRouteParamsList} from '../../Navigator/NavigationTypes';
import {Box, Text} from 'react-native-design-utility';
import {getIcons} from '../../utils/helper';
import {theme} from '../../constant/theme';

type NavigationParams = RouteProp<SearchStackRouteParamsList, 'PodCastDetails'>;
const Icon = getIcons();

const PodCastDetails = () => {
  const {data: PodCastData} = useRoute<NavigationParams>().params ?? {};

  return (
    <Box f={1} bg="white">
      <FlatList
        ItemSeparatorComponent={() => (
          <Box width="100%" px="sm" my="sm">
            <Box style={{height: StyleSheet.hairlineWidth}} bg="grey" mt={10} />
          </Box>
        )}
        ListHeaderComponent={
          <>
            <Box
              margin={10}
              justify="start"
              flexDirection="row"
              borderRadius={10}>
              {/* {data.thumbnail && ( */}
              <Box mr={10} center>
                <Image
                  testID="img"
                  source={{uri: PodCastData.thumbnail}}
                  style={styles.img}
                />
              </Box>
              {/* )} */}
              <Box f={1}>
                <Text size="base" bold numberOfLines={2} ellipsizeMode="tail">
                  {PodCastData.podcastName}
                </Text>
                <Text size="xsm" color="grey">
                  {PodCastData.artist}
                </Text>
                <Text color="blueLight" size="xsm">
                  Subscribe
                </Text>
              </Box>
            </Box>

            <Box px="sm" mb="md" dir="row" align="center">
              <Box mr={10}>
                <Icon
                  name="play-circle"
                  size={38}
                  color={theme.color.blueLighter}
                />
              </Box>

              <Box>
                <Text bold>Play</Text>
                <Text size="xsm">#400- The Last Episode</Text>
              </Box>
            </Box>

            <Box px="sm" mb="md">
              <Text size="lg" bold>
                Episode
              </Text>
            </Box>
          </>
        }
        data={[{id: '1'}, {id: '2'}]}
        renderItem={() => (
          <>
            <Box px="sm">
              <Text size="base" color="grey" marginTop={12}>
                ABC
              </Text>
              <Text bold> The Title</Text>
              <Text
                size="sm"
                color="grey"
                marginTop={2}
                numberOfLines={3}
                ellipsizeMode="tail">
                While React Navigation 6 doesn't introduce changes of the same
                magnitude as React Navigation 5, there are still some breaking
                changes. It is possible, however, to mix packages from React
                Navigation 5 and React Navigation 6 (with a few caveats) so that
                you can gradually upgrade packages.
              </Text>
              <Text size="xsm" color="blue">
                3hrs 13min
              </Text>
            </Box>
          </>
        )}
        keyExtractor={item => item.id}
      />
    </Box>
  );
};

const styles = StyleSheet.create({
  img: {
    height: 70,
    width: 70,
    borderRadius: 60,
    margin: 2,
  },
});
export default PodCastDetails;
