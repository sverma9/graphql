/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Box} from 'react-native-design-utility';
import {theme} from '../../constant/theme';
import {getIcons} from '../../utils/helper';

const LibraryScreen = () => {
  const Icon = getIcons();
  return (
    <SafeAreaView style={styles.container}>
      <Box f={1} center>
        <Icon
          name="bitbucket-square"
          size={80}
          color={theme.color.blueLightest}
        />
      </Box>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
  },
});
export default LibraryScreen;
